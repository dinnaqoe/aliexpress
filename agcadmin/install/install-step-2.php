									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header green lighter bigger"><i class="ace-icon fa fa-user blue"></i> ADMIN LOGIN</h4>

											<div class="space-6"></div>
											<form method="POST">
												<fieldset>
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="text" name="aff_username" class="form-control" placeholder="Username" />
														</span>
													</label>

													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="password" name="aff_password" class="form-control" placeholder="Password" />
														</span>
													</label>

													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="text" name="aff_idaff" class="form-control" placeholder="ID Affiliate [1234]" />
														</span>
													</label>

													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<select name="aff_idoffer" class="form-control">
																<option value="55">55 - Consumer Electronic</option>
																<option value="68">68 - Top Picks</option>
																<option value="70">70 - Dresses</option>
																<option value="75">75 - Computers</option>
															</select>
														</span>
													</label>

													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input type="text" name="aff_subaff" class="form-control" placeholder="Sub Affiliate [newalibaba]" />
														</span>
													</label>

													<div class="space-24"></div>

													<div class="clearfix">
														<button type="submit" class="width-65 pull-right btn btn-sm btn-success">
															<span class="bigger-110">Submit</span>
														</button>
													</div>
												</fieldset>
											</form>
										</div>
									</div><!-- /.widget-body -->