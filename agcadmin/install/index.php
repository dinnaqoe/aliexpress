<?php include('head.php'); ?>

	<body class="login-layout light-login">
		<div class="main-container">
			<div class="main-content">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="login-container">
							<div class="center">
								<h1>
									<span class="red">AGC</span>
									<span class="white" id="grey">SETTING</span>
								</h1>
							</div>

							<div class="space-6"></div>

							<div class="position-relative">
								<div id="signup-box" class="signup-box visible widget-box no-border">
									<?php 
									if(isset($_GET['step'])){
										if($_GET['step'] == TRUE){
											if($idAff == '0000' || $categoryAff == '00'){
												include('install-step-2.php');
											}else{
												include('install-step-1.php');
											}
										}else{
											include('install-step-1.php');
										}
									}else{
										include('install-step-1.php');
									}
									?>
								</div>
							</div><!-- /.position-relative -->

						</div>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.main-content -->
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->
		<script type="text/javascript">
			window.jQuery || document.write("<script src='./../assets/js/jquery.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='./../assets/js/jquery1x.js'>"+"<"+"/script>");
</script>
<![endif]-->
		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='./../assets/js/jquery.mobile.custom.js'>"+"<"+"/script>");
		</script>

	</body>
</html>