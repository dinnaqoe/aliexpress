<div class="wrapper_content">
               <div class="container_24 ">
                  <div class="clear"></div>
                  <div class="grid_24 em-breadcrumbs">
                     <div class="breadcrumbs">
                        <ul>
                           <li class="home">
                              <a href="<?php echo $realDomain; ?>" title="Go to Home Page">Home</a>
                              <span class="separator">/ </span>
                           </li>
                           <li class="category10">
                              <strong>category </strong>
							  <span class="separator">/ </span>
                           </li>
                           <li class="category10">
                              <strong><?php echo $titleCat; ?></strong>
                           </li>
                        </ul>
                     </div>
                  </div>
                  <div class="grid_18 push_6 em-main-wrapper">
					 <p style="margin:0 0 27px;" class="category-image"></p>
                     <div class="page-title category-title">
                        <h1><?php echo $titleCat; ?></h1>
                     </div>
                     <div class="category-list-products">
                        
                        <div class="category-products" id="category-products-ajax">
							  <?php 		  
							  for($gr=0;$gr<$xCount;$gr++){ 
								$st = $gr*3;
								if($gr == ($xCount-1)){
								  $ls = $st+$yCount;
								}else{
								  $ls = $st+3;
								}
							  ?>
						   <ol class="products-list list-infinite" id="products-list"> 
							  <?php for($c=$st;$c<$ls;$c++){ ?>
							  <li class="item-products item box  odd">
								 <a href="./product.html" title="Ottoman" class="product-image">
								 <!--<span class="productlabels_icons">
								 <span class="label bestseller">
								 <span>
								 Best        </span>
								 </span>
								 </span>-->                
								 <img width="225px;" height="225px;" src="<?php echo $tagTumb[$c]; ?>" rel="image" data-alt-src="<?php echo $tagTumb[$c]; ?>" alt="<?php echo $tagTitle[$c]; ?>"></a>
								 <div class="product-shop">
									<div class="f-fix">
									   <div class="ratings">
										  <div class="rating-box">
											 <div class="rating" style="width:<?php echo rand(20,100)?>%"></div>
										  </div>
										  <!--<span class="amount"><a href="#" onclick="var t = opener ? opener.window : window; t.location.href='./review/product/list/id/51/category/10/'; return false;">2 Review(s)</a></span>-->
									   </div>
									   <h2 class="product-name">
									   <a href="<?php echo $tagSingle[$c]; ?>" title="<?php echo $tagTitle[$c]; ?>">
										<?php 
											preg_match('/^(?>\S+\s*){1,3}/', $tagTitle[$c], $match);
											echo rtrim($match[0]);
										?>
										</a></h2>
									   <div class="desc std">
										  <?php echo $tagTitle[$c]; ?>
										  <br />
										  <a href="<?php echo $tagSingle[$c]; ?>" title="Ottoman" class="link-learn">Learn More</a>
									   </div>
									   <div class="price-box">
										  <span class="regular-price" id="product-price-51">
										  <span class="price">$<?php echo $tagRealPrice[$c]; ?></span></span>
									   </div>
									   <div class="actions">
										  <button type="button" title="Add to Cart" class="button btn-cart" onclick="window.location='<?php echo $tagSingle[$c]; ?>'" ><span><span>DETAILS</span></span></button>
										  <!--<ul class="add-to-links">
											 <li><span class="separator">|</span> <a href="./#" class="link-compare" title="Add to Compare">Add to Compare</a></li>
											 <li><a href="./login.html" class="link-wishlist" title="Add to Wishlist">Add to Wishlist</a></li>
										  </ul>-->
									   </div>
									</div>
								 </div>
							  </li>
							  <?php } ?>
						   </ol>						   
						    <?php } ?>
						  <div class="pagination pull-right">
							<style>
							.pagination  {}
							.pagination ul { padding:0 0 0 0px; }
							.pagination ul li{ 
								background: #fff none repeat scroll 0 0;
								float: left;
								margin: 2px;
								padding: 10px;
								text-align: center;
								width: 30px;
							}
							.pagination a:hover, .pagination .active a { background-color: #fff; color: #f25c27; }
							</style>
							<ul>
							  <?php if($noPage > 1){ ?>
							  <li><a href="<?php echo $urlPage . ($noPage-1) . $suffPerm; ?>">Prev</a></li>
							  <?php 
							  }
								for($p=1;$p<=$allCount;$p++){
									if ((($p >= $noPage - 3) && ($p <= $noPage + 3)) || ($p == 1) || ($p == $allCount)) {
										if ($p == $noPage) {
							  ?>
							  <li class="active"><a href="<?php echo $urlPage . $p . $suffPerm; ?>"><?php echo $p; ?></a></li>
							  <?php }else{ ?>
							  <li><a href="<?php echo $urlPage . $p . $suffPerm;; ?>"><?php echo $p; ?></a></li>
							  <?php 
										}
									}
								}
							  if ($noPage < $allCount) {
							  ?>
							  <li><a href="<?php echo $urlPage . ($noPage+1) . $suffPerm; ?>">Next</a></li>
							  <?php } ?>
							</ul>
						  </div>
						</div>
                        <script type="text/javascript">
                           if($$('.toolbar .toolbar-option .pages ol li a')){
                           	jQuery('#category-products-ajax .list-infinite').infinitescroll({
                           		navSelector  	: "#category-products-ajax-next:last",
                           		nextSelector 	: "#category-products-ajax-next:last",
                           		itemSelector 	: "#category-products-ajax .list-infinite li.item",
                           		dataType	 	: 'html',
                                      loading: {
                           img				: "./skin/galabigshop/images/ajax-loader.gif",
                           },
                           		maxPage         : $$('.toolbar-bottom .toolbar .toolbar-option .pages ol li a').length,
                           		                			state			: {
                           			isPaused : true
                           		},
                           		                			path: function(index) {
                           			return './ajaxproduct_category_list.html';
                           		}
                           	}, function(newElements, data, url){
                           		                				jQuery("#category-products-ajax-next:last").show();
                           		                			setTimeout(function(){
                           		    afterLoadAjax('#category-products-ajax');
                                      },500);
                           	});
                           	                		jQuery('#category-products-ajax-next').click(function(){
                           		jQuery('#category-products-ajax .list-infinite').infinitescroll('retrieve');
                           		return false;
                           	});
                           	                	}	
                        </script>
                     </div>
                  </div>
                  <?php include('sidebar.php');?>
                  <div class="clear"></div>
               </div>
            </div>