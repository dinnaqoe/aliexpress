<div id="maincontainer">
  <!-- Section Start-->
  <section id="product">
    <div class="container"> 
     <?php if($numVHome == TRUE){ ?>	
     <!--  breadcrumb -->  
      <ul class="breadcrumb">
        <li>
          <a href="<?php echo $realDomain; ?>">Home</a>
          <span class="divider">/</span>
        </li>
		<?php if($itemSubCat == '0'){ ?>
        <li class="active">
          <a href="<?php echo $urlCatItem; ?>"><?php echo $nameCatItem; ?></a>
        </li>
		<?php }else{ ?>
        <li>
          <a href="<?php echo $urlCatItem; ?>"><?php echo $nameCatItem; ?></a>
          <span class="divider">/</span>
        </li>
        <li class="active">
          <a href="<?php echo $urlSubCatItem; ?>"><?php echo $nameSubCatItem; ?></a>
        </li>
		<?php } ?>
      </ul>
	  <?php } ?>
      <div class="row">        
        <!-- Sidebar Start-->
        <?php include_once('sidebar.php'); ?>
        <!-- Sidebar End-->
        <?php if($numVHome == TRUE){ ?>
		<!-- Item-->
        <div class="span4 mt40">
          <ul class="thumbnails mainimage">
            <?php
			if($numImage >= 3){
				$fPic = 3;
			}else{
				$fPic = $numImage;
			}
				for($vp=0;$vp<$fPic;$vp++){
			?>
			<li class="span4">
              <a  rel="position: 'inside' , showTitle: false, adjustX:-4, adjustY:-4" class="thumbnail cloud-zoom" href="<?php echo $itemPict[$vp]; ?>">
                <img src="<?php echo $itemPict[$vp]; ?>" alt="<?php echo str_replace($HTMLascii,$HTMLreal,$itemTitle); ?>" title="<?php echo str_replace($HTMLascii,$HTMLreal,$itemTitle); ?>">
              </a>
            </li>
				<?php } ?>
          </ul>
          <span>Mouse move on Image to zoom</span>
		  <?php if($numImage > 1){ ?>
          <ul class="thumbnails mainimage" itemscope itemtype="http://schema.org/Product">
            <?php for($vp=0;$vp<$fPic;$vp++){ ?>
			<li class="producthtumb">
              <a class="thumbnail" >
                <img src="<?php echo $itemTumb[$vp]; ?>" alt="<?php echo str_replace($HTMLascii,$HTMLreal,$itemTitle); ?>" title="<?php echo str_replace($HTMLascii,$HTMLreal,$itemTitle); ?>">
              </a>
            </li>
			<?php } ?>
          </ul>
		  <?php } ?>
        </div>
        <!-- Right Details-->
        <div class="span5 mt40">
          <div class="row">
            <div class="span5">
              <h1 class="productname" itemscope itemtype="http://schema.org/Product"><span class="bgnone" itemprop="name"><?php echo $itemTitle; ?></span></h1>
              <div class="productprice" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                <?php if($itemDiscPrice == TRUE){ ?>
				<div class="productpageprice" itemprop="priceCurrency" content="USD">
                  <span class="spiral"></span><span itemprop="price" content="<?php echo $itemDiscPrice; ?>">$<?php echo $itemDiscPrice; ?></span>
				</div>
                <div class="productpageoldprice">Old price : $<?php echo $itemRealPrice; ?></div>
				<?php }else{ ?>
				<div class="productpageprice" itemprop="priceCurrency" content="USD">
                  <span class="spiral"></span><span itemprop="price" content="<?php echo $itemRealPrice; ?>">$<?php echo $itemRealPrice; ?></span>
				</div>
				<?php } ?>
				<?php if($itemRating == TRUE){ ?>
                <ul class="rate">
                  <?php 
					$conR = 5-$itemRating;
					for($or=0;$or<$itemRating;$or++){ 
						echo '<li class="on"></li>';
					}
					if($conR != '0'){
						for($fr=0;$fr<$conR;$fr++){ 
							echo '<li class="off"></li>';
						}
					}
				?>
                </ul>
				<?php } ?>
              </div>
              <ul class="productpagecart" style="margin-top:50px;">
                <li><a class="wish" itemprop="availability" href="#cart" onclick="affClick()"><?php echo $cpaButton; ?></a></li>
              </ul>
            </div>
          </div>
        </div>
		<?php if($caption == TRUE){ ?>
		<div class="span9">
			<div class="caption">
				<span itemprop="description"><?php echo $singleDesc; ?></span>
			</div>
		</div>
		<?php } ?>
		   <?php if($numSpec == TRUE){ ?>
		   <!-- Product Description tab & comments-->
		  <section class="span9">
			<ul class="nav nav-tabs" id="myTab">
			  <li class="active"><a href="#specification">Specification</a></li>
			</ul>
			<div class="tab-content">
			  <div class="tab-pane active" id="specification">
				<ul class="productinfo">
				  <?php for($s=0;$s<$numSpec;$s++){ ?>
				  <li><span class="productinfoleft"> <?php echo $specName[$s]; ?>:</span> <?php echo $specValue[$s]; ?> </li>
				  <?php } ?>
				</ul>
			  </div>
			</div>
		  </section>
		  <?php } ?>
		  <div class="span9">
			<section id="featured" class="row mt40">
			  <h1 class="heading1 mt0"><span class="maintext">More Products</span></h1>
			  <ul class="thumbnails">
				<?php for($mp=0;$mp<3;$mp++){ ?>
				<li class="span3">
				  <a class="prdocutname" href="<?php echo $mItemSource[$mp]; ?>"><?php echo substr($mItemTitle[$mp],0,25); ?>..</a>
				  <div class="thumbnail">
					<a href="<?php echo $mItemSource[$mp]; ?>"><img src="<?php echo $mItemTumb[$mp]; ?>" alt="<?php echo str_replace($HTMLascii,$HTMLreal,$mItemTitle[$mp]); ?>" title="<?php echo str_replace($HTMLascii,$HTMLreal,$mItemTitle[$mp]); ?>"></a>
					<div class="pricetag">
					  <span class="spiral"></span><a href="<?php echo $mItemSource[$mp]; ?>" class="productcart">DETAILS</a>
					  <div class="price">
						<?php if($mItemDiscPrice[$mp] == TRUE){ ?>
						<div class="pricenew">$<?php echo $mItemDiscPrice[$mp]; ?></div>
						<div class="priceold">$<?php echo $mItemRealPrice[$mp]; ?></div>
						<?php }else{ ?>
						<div class="pricenew">$<?php echo $mItemRealPrice[$mp]; ?></div>
						<?php } ?>
					  </div>
					</div>
				  </div>
				</li>
				<?php } ?>
			  </ul>
			</section>
		  </div>
		  <div class="span9">
			<section id="featured" class="row mt40">
			  <h1 class="heading1 mt0"><span class="maintext">Featured Products</span><span class="subtext"> See Our Most featured Products</span></h1>
			  <ul class="thumbnails">
				<?php for($ri=0;$ri<3;$ri++){ ?>
				<li class="span3">
				  <a class="prdocutname" href="<?php echo $reSingle[$ri]; ?>"><?php echo substr($reTitle[$ri],0,25); ?>..</a>
				  <div class="thumbnail">
					<a href="<?php echo $reSingle[$ri]; ?>"><img src="<?php echo $reTumb[$ri]; ?>" alt="<?php echo str_replace($HTMLascii,$HTMLreal,$reTitle[$ri]); ?>" title="<?php echo str_replace($HTMLascii,$HTMLreal,$reTitle[$ri]); ?>"></a>
					<div class="pricetag">
					  <span class="spiral"></span><a href="<?php echo $reSingle[$ri]; ?>" class="productcart">DETAILS</a>
					  <div class="price">
						<?php if($reDiscPrice[$ri] == TRUE){ ?>
						<div class="pricenew">$<?php echo $reDiscPrice[$ri]; ?></div>
						<div class="priceold">$<?php echo $reRealPrice[$ri]; ?></div>
						<?php }else{ ?>
						<div class="pricenew">$<?php echo $reRealPrice[$ri]; ?></div>
						<?php } ?>
					  </div>
					</div>
				  </div>
				</li>
				<?php } ?>
			  </ul>
			</section>
		  </div>
      </div>
		<?php } ?>
    </div>
  </section>
</div>
<!-- /maincontainer -->